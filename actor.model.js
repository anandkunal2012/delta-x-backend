const mongoose = require('mongoose');
const Schema = mongoose.Schema;

let Actor = new Schema({
    name: {
        type: String
    },
    sex: {
        type: String
    },
    dob: {
        type: String
    },
    bio: {
        type: String
    }
});

module.exports = mongoose.model('Actor', Actor);