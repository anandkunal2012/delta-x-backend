const mongoose = require('mongoose');
const Schema = mongoose.Schema;

let Movie = new Schema({
    name: {
        type: String
    },
    yor: {
        type: String
    },
    poster: {
        type: String
    },
    plot: {
        type: String
    },
    description: {
        type: String
    },
    cast: {
        type: Array
    }
});

module.exports = mongoose.model('Movie', Movie);